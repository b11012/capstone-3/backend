const mongoose = require('mongoose');

/*
	name- string,
	description- string,
	price- number,
	isActive- boolean,
			default: true
	createdOn: date
	enrollees: [
		{
			userId: string,
			status: string,
			dateEnrolled: date
		}
	]
*/

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Course name is required"]
	},

	description: {
		type: String,
		required: [true, "Course description is required"]
	},

	price: {
		type: Number,
		required: [true, "Course price is required"]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	}

})

module.exports = mongoose.model("Product", productSchema);
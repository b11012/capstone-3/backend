// [SECTION] DEPENDENCIES 
const Product = require("../models/Product");

// [SECTION] ADD PRODUCT
module.exports.addProduct = (req,res) => {

	console.log(req.body);

	let newProduct = new Product({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	})

	newProduct.save()
	.then(product => res.send(product))
	.catch(error => res.send(error))
}

// [SECTION] GET ALL COURSES
module.exports.getAllProduct = (req,res) => {

	Product.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));

}

// [SECTION] GET SINGLE COURSE
module.exports.getSingleProduct = (req,res) => {


	Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))

}
// [SECTION] UPDATING A COURSE

module.exports.updateProduct = (req, res) => {

	let updates = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Product.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(err => res.send(err))
}

// [SECTION] ARCHIVE A COURSE
module.exports.archive = (req,res) => {

	//console.log(req.params.id);//course's id

	//updates object will contain the field/fields to update and its new value.
	let updates = {
		isActive: false
	}

	Product.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(err => res.send(err));

}

// [SECTION] ACTIVATE A COURSE
module.exports.activate = (req,res) => {

	
	let updates = {
		isActive: true
	}

	Product.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(err => res.send(err));

}

// [SECTION] GET ALL ACTIVE COURSES
module.exports.getActiveProduct = (req,res) => {

	Product.find({isActive: true})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

// [SECTION] FIND COURSES BY NAME
module.exports.findProductByName = (req, res) => {

	console.log(req.body) // contain the name of the course you are looking for

	Course.find({name: {$regex: req.body.name, $options: '$i'}})
	.then(result => {

		if(result.length === 0){
			return res.send('No courses found')
		
		} else {

			return res.send(result)
		}
	})
}

// RETRIEVE ALL ACTIVE PRODUCTS

module.exports.retrieveActiveProducts = (req, res) => {

	console.log(req.body)

	Product.find({isActive: true})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// DELETE A PRODUCT

module.exports.deleteProduct = (req, res) => {

	console.log(req.params);

	Product.findByIdAndDelete(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};
